local lspconfig = require("lspconfig")
local cmp_nvim_lsp = require("cmp_nvim_lsp")
local mason_lspconfig = require("mason-lspconfig")
local on_attach = require("plugins.lspconfig").on_attach

local function generate_opts()
    local capabilities = cmp_nvim_lsp.default_capabilities()

    local opts = {
        on_attach = on_attach,
        capabilities = capabilities,
        flags = {
            debounce_text_changes = 150,
        },
    }

    return opts
end

mason_lspconfig.setup()
mason_lspconfig.setup_handlers({
    function(server_name)
        local opts = generate_opts()
        lspconfig[server_name].setup(opts)
    end,
})
