require("focus").setup({
	excluded_filetypes = { "toggleterm" },
	excluded_buftypes = { "nofile", "prompt", "help", "terminal" },
	bufnew = false,
	cursorline = false,
	signcolumn = false,
	number = false,
	relativenumber = false,
	hybridnumber = false,
	winhighlight = true,
})
