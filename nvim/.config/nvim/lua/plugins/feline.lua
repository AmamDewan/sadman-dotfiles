local api = vim.api
local fn = vim.fn
local tbl_extend = vim.tbl_extend
local tbl_deep_extend = vim.tbl_deep_extend
local feline = require("feline")
local feline_provider_file = require("feline.providers.file")
local navic = require("nvim-navic")
local lsp_utils = require("lsp.utils")

local colors = require("theme").colors
local utils = require("utils")

local mode_table_base_item = {
    color_fg1 = colors.bg0,
    color_fg2 = colors.fg0,
}
local extend_mode_table_base_item = utils.generate_tbl_extend_function("force", mode_table_base_item)
local mode_table = {
    [110] = extend_mode_table_base_item({
        text = "NORMAL",
        color_bg1 = colors.grey2,
        color_bg2 = colors.bg2,
    }),
    [105] = extend_mode_table_base_item({
        text = "INSERT",
        color_bg1 = colors.blue_light,
        color_bg2 = colors.blue,
    }),
    [82] = extend_mode_table_base_item({
        text = "REPLACE",
        color_bg1 = colors.red_light,
        color_bg2 = colors.red,
    }),
    ["Rv"] = extend_mode_table_base_item({
        text = "VIRTUAL",
        color_bg1 = colors.purple_light,
        color_bg2 = colors.purple,
    }),
    [118] = extend_mode_table_base_item({
        text = "VISUAL",
        color_bg1 = colors.orange_light,
        color_bg2 = colors.orange,
    }),
    [86] = extend_mode_table_base_item({
        text = "V-LINE",
        color_bg1 = colors.orange_light,
        color_bg2 = colors.orange,
    }),
    [22] = extend_mode_table_base_item({
        text = "V-BLOCK",
        color_bg1 = colors.orange_light,
        color_bg2 = colors.orange,
    }),
    [99] = extend_mode_table_base_item({
        text = "COMMAND",
        color_bg1 = colors.yellow_light,
        color_bg2 = colors.yellow,
    }),
    [115] = extend_mode_table_base_item({
        text = "SELECT",
        color_bg1 = colors.aqua_light,
        color_bg2 = colors.aqua,
    }),
    [83] = extend_mode_table_base_item({
        text = "S-LINE",
        color_bg1 = colors.aqua_light,
        color_bg2 = colors.aqua,
    }),
    [19] = extend_mode_table_base_item({
        text = "S-BLOCK",
        color_bg1 = colors.aqua_light,
        color_bg2 = colors.aqua,
    }),
    [116] = extend_mode_table_base_item({
        text = "TERMINAL",
        color_bg1 = colors.green_light,
        color_bg2 = colors.green,
    }),
    ["rm"] = extend_mode_table_base_item({
        text = "--MORE",
        color_bg1 = colors.grey2,
        color_bg2 = colors.bg2,
    }),
}

local function get_mode()
    return api.nvim_get_mode().mode
end

local function get_mode_data(mode_name)
    local mode_name_byte = mode_name:byte()
    local mode = mode_table[mode_name_byte]

    if mode == nil then
        mode = vim.tbl_extend("force", mode_table["rm"], {
            text = mode_name,
        })
    end

    return mode
end

local function space_provider(n)
    return string.rep(" ", n or 1)
end

local function format_file_path(path, len)
    local truncated_path = utils.truncate_path(path, len, true)

    if #path > #truncated_path then
        truncated_path = ".../" .. truncated_path
    end

    return truncated_path
end

local function vi_mode_hl()
    local mode_name = get_mode()
    local mode_data = get_mode_data(mode_name)
    return {
        fg = mode_data.color_fg1,
        bg = mode_data.color_bg1,
        style = "bold",
        name = "StatuslineViMode",
    }
end

local primary_hl = {
    fg = colors.fg0,
    bg = colors.bg2,
}

local secondary_hl = {
    fg = colors.fg1,
    bg = colors.bg1,
}

local primary_winbar_hl = {
    fg = colors.grey0,
    bg = colors.bg0,
    style = "italic",
}

local primary_winbar_inactive_hl = tbl_extend("force", primary_winbar_hl, {
    style = "none",
})

local file_path_hl = tbl_extend("force", primary_hl, {
    name = "StatuslineFilePath",
})

local file_path_winbar_hl = tbl_extend("force", primary_winbar_hl, {
    name = "WinbarFilePath",
})

local file_path_winbar_inactive_hl = tbl_extend("force", file_path_winbar_hl, primary_winbar_inactive_hl)

local file_size_hl = tbl_extend("force", primary_hl, {
    name = "StatuslineFileSize",
})

local file_type_hl = tbl_extend("force", primary_hl, {
    name = "StatuslineFileType",
})

local file_encoding_hl = tbl_extend("force", primary_hl, {
    name = "StatuslineFileEncoding",
})

local file_format_hl = tbl_extend("force", primary_hl, {
    name = "StatuslineFileFormat",
})

local git_branch_hl = tbl_extend("force", secondary_hl, {
    style = "bold",
    name = "StatuslineGitBranch",
})

local git_branch_icon_hl = tbl_extend("force", git_branch_hl, {
    fg = colors.red,
})

local git_diff_hl = tbl_extend("force", secondary_hl, {
    style = "bold",
    name = "StatuslineGitDiff",
})

local git_diff_added_icon_hl = tbl_extend("force", git_diff_hl, {
    fg = colors.green,
})

local git_diff_removed_icon_hl = tbl_extend("force", git_diff_hl, {
    fg = colors.red,
})

local git_diff_changed_icon_hl = tbl_extend("force", git_diff_hl, {
    fg = colors.yellow,
})

local code_context_winbar_hl = tbl_extend("force", primary_winbar_hl, {
    name = "WinbarCodeContext",
})

local lsp_client_names_hl = tbl_extend("force", secondary_hl, {
    fg = colors.fg0,
    name = "StatuslineLspClients",
})

local lsp_diagnostics_hl = tbl_extend("force", secondary_hl, {
    fg = colors.fg0,
    name = "StatuslineLspDiagnostics",
})

local lsp_diagnostics_icon_hl = tbl_extend("force", lsp_diagnostics_hl, {
    style = "bold",
})

local lsp_diagnostics_errors_icon_hl = tbl_extend("force", lsp_diagnostics_icon_hl, {
    fg = colors.red,
})

local lsp_diagnostics_warnings_icon_hl = tbl_extend("force", lsp_diagnostics_icon_hl, {
    fg = colors.yellow,
})

local lsp_diagnostics_info_icon_hl = tbl_extend("force", lsp_diagnostics_icon_hl, {
    fg = colors.blue,
})

local lsp_diagnostics_hints_icon_hl = tbl_extend("force", lsp_diagnostics_icon_hl, {
    fg = colors.green,
})

local lsp_client_names_icon_hl = tbl_extend("force", secondary_hl, {
    fg = colors.aqua,
    style = "bold",
    name = "StatuslineLspClients",
})

local cursor_info_hl = tbl_extend("force", primary_hl, {
    name = "StatuslineCursorInfo",
})

local vi_mode_component = {
    name = "vi_mode",
    provider = function()
        local mode_name = get_mode()
        local mode_data = get_mode_data(mode_name)
        return mode_data.text
    end,
    hl = vi_mode_hl,
}

local file_path_component = {
    provider = function()
        local path = fn.expand("%:p")
        local truncated_path = format_file_path(path, 5)

        return truncated_path
    end,
    hl = file_path_hl,
}

local file_path_winbar_component = {
    provider = function()
        local path = fn.expand("%")
        local truncated_path = format_file_path(path, 3)

        return truncated_path
    end,
    hl = file_path_winbar_hl,
}

local file_path_winbar_inactive_component = tbl_extend("force", file_path_winbar_component, {
    hl = file_path_winbar_inactive_hl,
})

local file_size_component = {
    provider = "file_size",
    hl = file_size_hl,
}

local git_branch_component = {
    provider = "git_branch",
    hl = git_branch_hl,
    icon = {
        str = " ",
        hl = git_branch_icon_hl,
    },
}

local git_diff_added_component = {
    provider = "git_diff_added",
    hl = git_branch_hl,
    icon = {
        str = " 樂",
        hl = git_diff_added_icon_hl,
    },
}

local git_diff_removed_component = {
    provider = "git_diff_removed",
    hl = git_branch_hl,
    icon = {
        str = "  ",
        hl = git_diff_removed_icon_hl,
    },
}

local git_diff_changed_component = {
    provider = "git_diff_changed",
    hl = git_branch_hl,
    icon = {
        str = " ﱣ ",
        hl = git_diff_changed_icon_hl,
    },
}

local code_context_component = {
    provider = navic.get_location,
    hl = code_context_winbar_hl,
    enabled = navic.is_available,
}

local lsp_diagnostics_errors_component = {
    provider = "diagnostic_errors",
    hl = lsp_diagnostics_hl,
    icon = {
        str = "  ",
        hl = lsp_diagnostics_errors_icon_hl,
    },
}

local lsp_diagnostics_warnings_component = {
    provider = "diagnostic_warnings",
    hl = lsp_diagnostics_hl,
    icon = {
        str = "  ",
        hl = lsp_diagnostics_warnings_icon_hl,
    },
}

local lsp_diagnostics_info_component = {
    provider = "diagnostic_info",
    hl = lsp_diagnostics_hl,
    icon = {
        str = "  ",
        hl = lsp_diagnostics_info_icon_hl,
    },
}

local lsp_diagnostics_hints_component = {
    provider = "diagnostic_hints",
    hl = lsp_diagnostics_hl,
    icon = {
        str = "  ",
        hl = lsp_diagnostics_hints_icon_hl,
    },
}

local lsp_client_names_component = {
    provider = function()
        return lsp_utils.get_formatted_lsp_client_names()
    end,
    hl = lsp_client_names_hl,
    icon = {
        str = " 漣",
        hl = lsp_client_names_icon_hl,
    },
}

local file_type_component = {
    provider = {
        name = "file_type",
        opts = {
            case = "lowercase",
        },
    },
    hl = file_type_hl,
    icon = "  ",
}

local file_type_component_inactive = tbl_deep_extend("force", file_type_component, {
    provider = {
        opts = {
            case = "titlecase",
        },
    },
    icon = "",
})

local file_encoding_component = {
    provider = function()
        return feline_provider_file.file_encoding():lower()
    end,
    hl = file_encoding_hl,
    icon = "  ",
}

local file_format_component = {
    provider = function()
        return feline_provider_file.file_format():lower()
    end,
    hl = file_format_hl,
    icon = "  ",
}

local position_component = {
    provider = "position",
    hl = cursor_info_hl,
    icon = "  ",
}

local line_percent_component = {
    provider = "line_percentage",
    hl = cursor_info_hl,
    icon = "  ",
}

local components = {
    active = {},
    inactive = {},
}

local winbar_components = {
    active = {},
    inactive = {},
}

table.insert(components.active, {}) -- statusline active left section
table.insert(components.active, {}) -- statusline active right section

table.insert(components.inactive, {}) -- statusline inactive left section

-- statusline active components [left section]
table.insert(components.active[1], { provider = space_provider(), hl = vi_mode_hl })
table.insert(components.active[1], vi_mode_component)
table.insert(components.active[1], { provider = space_provider(), hl = vi_mode_hl })
table.insert(components.active[1], { provider = space_provider(), hl = file_path_hl })
table.insert(components.active[1], file_path_component)
table.insert(components.active[1], { provider = space_provider(), hl = file_size_hl })
table.insert(components.active[1], file_size_component)
table.insert(components.active[1], { provider = space_provider(), hl = file_size_hl })
table.insert(components.active[1], { provider = space_provider(), hl = git_branch_hl })
table.insert(components.active[1], git_branch_component)
table.insert(components.active[1], git_diff_added_component)
table.insert(components.active[1], git_diff_removed_component)
table.insert(components.active[1], git_diff_changed_component)

-- statusline active components [right section]
table.insert(components.active[2], lsp_diagnostics_errors_component)
table.insert(components.active[2], lsp_diagnostics_warnings_component)
table.insert(components.active[2], lsp_diagnostics_info_component)
table.insert(components.active[2], lsp_diagnostics_hints_component)
table.insert(components.active[2], lsp_client_names_component)
table.insert(components.active[2], { provider = space_provider(), hl = lsp_client_names_hl })
table.insert(components.active[2], file_type_component)
table.insert(components.active[2], file_encoding_component)
table.insert(components.active[2], file_format_component)
table.insert(components.active[2], position_component)
table.insert(components.active[2], line_percent_component)
table.insert(components.active[2], { provider = space_provider(), hl = cursor_info_hl })

-- statusline inactive components [left section]
table.insert(components.inactive[1], { provider = space_provider(), hl = file_type_hl })
table.insert(components.inactive[1], file_type_component_inactive)

table.insert(winbar_components.active, {}) -- winbar active left section
table.insert(winbar_components.inactive, {}) -- winbar inactive left section

-- winbar active components [left section]
table.insert(winbar_components.active[1], { provider = space_provider(), hl = file_path_winbar_hl })
table.insert(winbar_components.active[1], file_path_winbar_component)
table.insert(winbar_components.active[1], { provider = space_provider(), hl = code_context_winbar_hl })
table.insert(winbar_components.active[1], code_context_component)

-- winbar inactive components [left section]
table.insert(winbar_components.inactive[1], { provider = space_provider(), hl = file_path_winbar_inactive_hl })
table.insert(winbar_components.inactive[1], file_path_winbar_inactive_component)
table.insert(winbar_components.inactive[1], { provider = space_provider(), hl = code_context_winbar_hl })
table.insert(winbar_components.inactive[1], code_context_component)

feline.setup({
    components = components,
})

feline.winbar.setup({
    components = winbar_components,
    disable = {
        filetypes = {
            "^NvimTree$",
            "^packer$",
            "^startify$",
            "^fugitive$",
            "^fugitiveblame$",
            "^qf$",
            "^help$",
        },
        buftypes = {
            "^terminal$",
        },
        bufnames = {},
    },
})
