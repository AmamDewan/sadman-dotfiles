require("ibl").setup {
    exclude = {
        buftypes = { "help", "terminal", "nofile" },
        filetypes = { "packer", "alpha", "TelescopePrompt", "NvimTree" }
    },
    indent = {
        char = "│",
        highlight = { "Red", "Green", "Orange", "Blue", "Purple", "Aqua" }
    }
}
