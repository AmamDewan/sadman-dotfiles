pcall(require, "impatient")
pcall(require, "packer_compiled")

local status, packer = pcall(require, "plugins.packer")

if not status then
    vim.notify(
        "Packer is not installed. Please install it first from this link https://github.com/wbthomason/packer.nvim",
        "error"
    )
    return
end

local packer_util = require("packer.util")

return packer.startup({
    function(use)
        -- Impatient
        use({
            "lewis6991/impatient.nvim",
            as = "impatient",
        })
        -- Packer
        use({
            "wbthomason/packer.nvim",
            event = "VimEnter",
        })
        -- UI
        use({
            "freddiehaddad/feline.nvim",
            as = "feline",
            after = { "devicons", "navic" },
            config = function()
                require("plugins.feline")
            end,
        })
        use({
            "akinsho/nvim-bufferline.lua",
            branch = "main",
            as = "bufferline",
            after = "devicons",
            config = function()
                require("plugins.bufferline")
            end,
        })
        -- Theme
        use({
            "sainnhe/gruvbox-material",
            event = "BufEnter",
            config = function()
                require("plugins.gruvbox_material")
            end,
        })
        -- Icons
        use({
            "kyazdani42/nvim-web-devicons",
            as = "devicons",
            after = "gruvbox-material",
            config = function()
                require("plugins.devicons")
            end,
        })
        -- Colors
        use({
            "rrethy/vim-hexokinase",
            run = "make hexokinase",
            as = "hexokinase",
            event = "BufEnter",
            setup = require("plugins.setup").hexokinase(),
        })
        -- File managers and pickers
        use({
            "kyazdani42/nvim-tree.lua",
            as = "tree",
            module_pattern = "^nvim%-tree",
            config = function()
                require("plugins.tree")
            end,
        })
        use({
            "nvim-telescope/telescope.nvim",
            as = "telescope",
            module = "telescope",
            cmd = "Telescope",
            requires = {
                {
                    "nvim-telescope/telescope-file-browser.nvim",
                    as = "telescope-file-browser",
                },
                {
                    "nvim-telescope/telescope-fzf-native.nvim",
                    as = "telescope-fzf",
                    run = "make",
                    module = "fzf_lib",
                },
                {
                    "ahmedkhalf/project.nvim",
                    as = "project",
                    module = "project_nvim",
                    config = function()
                        require("plugins.project")
                    end,
                },
            },
            config = function()
                require("plugins.telescope")
            end,
        })
        -- LSP
        use({
            "neovim/nvim-lspconfig",
            as = "lspconfig",
            event = "BufEnter",
        })
        use({
            "williamboman/mason.nvim",
            as = "mason",
            config = function()
                require("mason").setup()
            end,
        })
        use({
            "williamboman/mason-lspconfig.nvim",
            as = "mason-lspconfig",
            after = { "lspconfig", "mason", "cmp", "navic" },
            config = function()
                require("plugins.mason_lspconfig")
            end,
        })
        use({
            "jose-elias-alvarez/null-ls.nvim",
            as = "null-ls",
            after = { "plenary", "lspconfig" },
        })
        use({
            "jayp0521/mason-null-ls.nvim",
            as = "mason-null-ls",
            after = "null-ls",
            config = function()
                require("plugins.mason_null_ls")
            end,
        })
        use({
            "onsails/lspkind-nvim",
            as = "lspkind",
            event = "BufEnter",
            config = function()
                require("plugins.lspkind")
            end,
        })
        use({
            "hrsh7th/nvim-cmp",
            as = "cmp",
            after = { "luasnip", "lspkind" },
            requires = {
                {
                    "hrsh7th/cmp-path",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-calc",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-buffer",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-omni",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-nvim-lsp",
                    as = "cmp-lsp",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-nvim-lsp-signature-help",
                    as = "cmp-lsp-signature-help",
                    after = "cmp",
                },
                {
                    "saadparwaiz1/cmp_luasnip",
                    as = "cmp-luasnip",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-nvim-lua",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-look",
                    after = "cmp",
                    config = function()
                        require("cmp").register_source("look", require("cmp_look").new())
                    end,
                },
                {
                    "hrsh7th/cmp-latex-symbols",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-emoji",
                    after = "cmp",
                },
                {
                    "hrsh7th/cmp-cmdline",
                    after = "cmp",
                },
            },
            config = function()
                require("plugins.cmp")
            end,
        })
        use({
            "L3MON4D3/LuaSnip",
            as = "luasnip",
            after = "friendly-snippets",
            requires = {
                "rafamadriz/friendly-snippets",
                as = "friendly-snippets",
                event = "BufEnter",
            },
        })
        use({
            "kosayoda/nvim-lightbulb",
            as = "lightbulb",
            after = "lspconfig",
            config = function()
                require("plugins.lightbulb")
            end,
        })
        use({
            "simrat39/symbols-outline.nvim",
            as = "symbols-outline",
            module = "symbols-outline",
            cmd = { "SymbolsOutline", "SymbolsOutlineOpen" },
            setup = function()
                require("symbols-outline").setup()
            end,
        })
        -- Treesitter
        use({
            "nvim-treesitter/nvim-treesitter",
            run = ":TSUpdate",
            as = "treesitter",
            event = "BufEnter",
            config = function()
                require("plugins.treesitter")
            end,
        })
        use({
            "nvim-treesitter/nvim-treesitter-refactor",
            as = "treesitter-refactor",
            after = "treesitter",
        })
        use({
            "romgrk/nvim-treesitter-context",
            as = "treesitter-context",
            after = "treesitter",
            config = function()
                require("plugins.treesitter_context")
            end,
        })
        use({
            "JoosepAlviste/nvim-ts-context-commentstring",
            branch = "main",
            as = "treesitter-context-commentstring",
            after = "treesitter",
        })
        use({
            "p00f/nvim-ts-rainbow",
            as = "treesitter-rainbow",
            after = "treesitter",
            config = function()
                require("plugins.treesitter_rainbow")
            end,
        })
        use({
            "andymass/vim-matchup",
            as = "matchup",
            after = "treesitter",
            config = function()
                require("plugins.matchup")
            end,
        })
        use({
            "windwp/nvim-autopairs",
            as = "autopairs",
            after = { "cmp", "treesitter" },
            config = function()
                require("plugins.autopairs")
            end,
        })
        use({
            "windwp/nvim-ts-autotag",
            as = "treesitter-autotag",
            after = "treesitter",
        })
        use({
            "SmiteshP/nvim-navic",
            as = "navic",
            after = "lspconfig",
            config = function()
                require("plugins.navic").setup()
            end,
        })
        -- Git
        use({
            "lewis6991/gitsigns.nvim",
            branch = "main",
            as = "gitsigns",
            after = "plenary",
            config = function()
                require("plugins.gitsigns")
            end,
        })
        use({
            "tpope/vim-fugitive",
            as = "fugitive",
            cmd = "G*",
        })
        -- Remote
        use({
            "chipsenkbeil/distant.nvim",
            as = "distant",
            cmd = "DistantLaunch",
            config = function()
                require("plugins.nvim")
            end,
        })
        -- Utilities
        use({
            "nvim-lua/plenary.nvim",
            as = "plenary",
            event = "BufEnter",
        })
        use({
            "rcarriga/nvim-notify",
            as = "notify",
            after = "gruvbox-material",
            config = function()
                require("plugins.notify")
            end,
        })
        use({
            "beauwilliams/focus.nvim",
            as = "focus",
            module = "focus",
            cmd = { "FocusSplitNicely", "FocusSplitLeft", "FocusSplitDown", "FocusSplitUp", "FocusSplitRight" },
            config = function()
                require("plugins.focus")
            end,
        })
        use({
            "dstein64/nvim-scrollview",
            as = "scrollview",
            event = "BufEnter",
            config = function()
                require("plugins.scrollview")
            end,
        })
        use({
            "goolord/alpha-nvim",
            as = "alpha",
            event = "VimEnter",
            config = function()
                require("plugins.alpha")
            end,
        })
        use({
            "lukas-reineke/indent-blankline.nvim",
            as = "indent-blankline",
            event = "BufEnter",
            main = "ibl",
            config = function()
                require("plugins.indent_blankline")
            end,
        })
        use({
            "windwp/nvim-spectre",
            as = "spectre",
            module = "spectre",
            config = function()
                require("spectre").setup({
                    live_update = true,
                })
            end,
        })
        use({
            "akinsho/toggleterm.nvim",
            branch = "main",
            as = "toggleterm",
            module = "toggleterm",
            cmd = "ToggleTerm",
            config = function()
                require("plugins.toggleterm")
            end,
        })
        use({
            "folke/persistence.nvim",
            as = "persistence",
            event = "BufReadPre",
            module = "persistence",
            config = function()
                require("persistence").setup()
            end,
        })
        use({
            "numToStr/Comment.nvim",
            as = "comment",
            after = "treesitter-context-commentstring",
            config = function()
                require("plugins.comment")
            end,
        })
        use({
            "wfxr/minimap.vim",
            as = "minimap",
            cmd = "MinimapToggle",
            config = function()
                require("plugins.minimap")
            end,
        })
        use({
            "mg979/vim-visual-multi",
            as = "visual-multi",
            event = "BufEnter",
        })
        use({
            "tpope/vim-surround",
            as = "sorround",
            event = "BufEnter",
        })
        use({
            "karb94/neoscroll.nvim",
            as = "neoscroll",
            event = "BufEnter",
            config = function()
                require("plugins.neoscroll")
            end,
        })
        use({
            "phaazon/hop.nvim",
            as = "hop",
            module = "hop",
            cmd = { "HopWord", "HopLine" },
            config = function()
                require("hop").setup()
            end,
        })
        use({
            "machakann/vim-highlightedyank",
            as = "highlightedyank",
            event = "BufEnter",
        })
        use({
            "folke/todo-comments.nvim",
            as = "todo-comments",
            after = "plenary",
            config = function()
                require("plugins.todo_comments")
            end,
        })
        use({
            "gpanders/editorconfig.nvim",
            as = "editorconfig",
            event = "BufEnter",
        })
    end,
    -- Config
    config = {
        -- Move to lua dir so impatient.nvim can cache it
        compile_path = packer_util.join_paths(vim.fn.stdpath("config"), "lua", "packer_compiled.lua"),
    },
})
