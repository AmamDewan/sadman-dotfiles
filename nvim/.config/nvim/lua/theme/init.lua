local M = {}

M.colors = {
    bg0 = "#32302F",
    bg1 = "#3C3836",
    bg2 = "#504945",
    bg3 = "#665C54",
    fg0 = "#D4BE98",
    fg1 = "#DDC7A1",
    red = "#C14A4A",
    red_light = "#EA6962",
    green = "#6C782E",
    green_light = "#A9B665",
    blue = "#45707A",
    blue_light = "#7DAEA3",
    aqua = "#4C7A5D",
    aqua_light = "#89B482",
    yellow = "#B47109",
    yellow_light = "#D8A657",
    orange = "#C35E0A",
    orange_light = "#E78A4E",
    pink = "#F06292",
    purple = "#945E80",
    purple_light = "#D3869B",
    grey0 = "#7C6F64",
    grey1 = "#928374",
    grey2 = "#A89984",
    none = "NONE",
}

return M
