function le --description 'list files (with extended attributes and sizzes) with exa' --wraps exa
    command exa --color always --icons --long --group --group-directories-first --extended $argv
end
