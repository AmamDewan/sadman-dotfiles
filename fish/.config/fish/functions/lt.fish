function lt --description 'list files (with directory tree) with exa' --wraps exa
    command exa --color always --icons --long --all --group --group-directories-first --tree --level 4 $argv
end

